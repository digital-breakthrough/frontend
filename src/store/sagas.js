import { all } from 'redux-saga/effects';
import { authSagas } from '../containers/Auth/sagas';
import { partySagas } from '../containers/CreateParty/sagas';
import { partiesSagas } from '../containers/Parties/sagas';

export function* rootSagas() {
  yield all([
    authSagas(),
    partySagas(),
    partiesSagas(),
  ]);
}
