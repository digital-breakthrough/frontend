import { combineReducers } from 'redux';
import { auth } from '../containers/Auth/reducer';
import { parties } from '../containers/Parties/reducer';
import { party } from '../containers/CreateParty/reducer';

const appReducer = {
  ...auth,
  ...parties,
  ...party,
};

export const reducers = (state, action) => combineReducers(appReducer)(state, action);
