
import {
  BECOME_CITIZEN,
  USER_AUTHORIZE, USER_AUTHORIZE_SUCCESS, USER_LOGOUT, USER_REGISTER
} from './constants';

const initialState = {
  isLoading: true,
  isError: false,
  isAuth: false,

  id: 0,
  email: '',
  firstname: '',
  lastname: '',
  birthdate: '',
  marital_status: '',
  political_status: '',

  middlename: '',
  city: '',

  userName: '',
  isCitizen: false,
  phone: '',
  password: '',
  compname: '',
  working: '',
  sex: '',
  spass: '',
  npass: '',
  datepass: '',
  codepass: '',
  whompass: '',
  iparty: '',
  refcode: '',
};

export const storeName = 'auth';

export const auth = {
  [storeName]: (state = initialState, { type, payload }) => {
    switch (type) {
      case USER_AUTHORIZE:
        return {
          ...state,
          isLoading: false,
          isError: true,
        };
      case USER_AUTHORIZE_SUCCESS:
        return {
          ...state,
          id: payload.id,
          firstname: payload.firstname,
          lastname: payload.lastname,
          city: payload.city,
          birthdate: payload.birth_date,
          marital_status: payload.marital_status,
          political_status: payload.political_status,
        };
      case BECOME_CITIZEN:
        return {
          ...state,
          isCitizen: true,
        };
      case USER_LOGOUT:
        return {
          ...state,
          id: 0,
          userName: '',
        };
      case USER_REGISTER:
        return {
          ...state,
          ...payload,
        };
      default:
        return state;
    }
  },
};
