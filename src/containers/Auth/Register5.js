import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Col, Container, Form, FormGroup, Input, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import { userRegisterAction } from './actions';
import { bindActionCreators } from 'redux';

export const Register5Container = ({
 userRegister
}) => {
  const [political_status, setP1] = useState('');
  const [iparty, setP2] = useState('');
  const [refcode, setP4] = useState('');

  const onSubmit = (e) => {
    e.preventDefault();
    userRegister({
      political_status,
      iparty,
      refcode,
    }, true);
  };

  return (
    <Container>
      <h1 className="h1">
        Регистрация на сайте: шаг 5 - последний!
      </h1>
      <Row>
        <Col xs={5}>
          <Form onSubmit={onSubmit} autoComplete="off">
          <FormGroup>
              <Input type="text"
                     placeholder="Политические убеждения *"
                     value={political_status}
                     onChange={({ target }) => setP1(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="text"
                     placeholder="Интересующая партия"
                     value={iparty}
                     onChange={({ target }) => setP2(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="text"
                     placeholder="Введите реферальный код"
                     value={refcode}
                     onChange={({ target }) => setP4(target.value)}
              />
            </FormGroup>
            <div className="mt-5 d-flex justify-content-between">
              <Button color="link" tag={Link} to="/register4">
                Вернуться
              </Button>
              <Button color="primary" type="submit" disabled={!(political_status.length)}
              >
                Зарегистрироваться
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  userRegister: bindActionCreators(userRegisterAction, dispatch),
});

export const Register5 = connect(mapStateToProps, mapDispatchToProps)(Register5Container);
