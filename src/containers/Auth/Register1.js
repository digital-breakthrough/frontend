import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Col, Container, Form, FormGroup, Input, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import { history } from '../../history';
import { bindActionCreators } from 'redux';
import { userRegisterAction } from './actions';
import { storeName } from './reducer';
import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

export const Register1Container = ({ userRegister, slastname, sbirthdate, smiddlename, sfirstname }) => {
  const [lastname, setP1] = useState(slastname);
  const [firstname, setP2] = useState(sfirstname);
  const [middlename, setP3] = useState(smiddlename);
  const [birthdate, setP4] = useState(sbirthdate);

  return (
    <Container>
      <h1 className="h1">
        Регистрация на сайте: шаг 1
      </h1>
      <Row>
        <Col xs={5}>
          <Form autoComplete="off">
            <FormGroup>
              <Input type="text"
                     placeholder="Фамилия *"
                     value={lastname}
                     onChange={({ target }) => setP1(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="text"
                     placeholder="Имя *"
                     value={firstname}
                     onChange={({ target }) => setP2(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="text"
                     placeholder="Отчество"
                     value={middlename}
                     onChange={({ target }) => setP3(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <DatePicker
              placeholderText='Дата рождения'
                selected={birthdate}
                // onChangeRaw={event => setP4(event.target.value)}
                onChange={date => setP4(date)}
                customInput={<Input type="text"
                placeholder="Дата рождения"
                value={birthdate}
                // onChange={({ target }) => setP4(target.value)}
         />}
              />
              {/* <Input type="text"
                     placeholder="Дата рождения"
                     value={birthdate}
                     onChange={({ target }) => setP4(target.value)}
              /> */}
            </FormGroup>
            <div className="mt-5 d-flex justify-content-between">
              <Button color="link" tag={Link} to="/login">
                Уже есть профиль?
              </Button>
              <Button color="primary"
                      onClick={() => {
                        userRegister({
                          lastname, middlename, firstname, birthdate
                        });
                        history.push('/register2')
                      }}
                      type="submit" disabled={!(lastname.length && firstname.length)}>
                Далее
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

const mapStateToProps = state => ({
  sfirstname: state[storeName].firstname,
  smiddlename: state[storeName].middlename,
  slastname: state[storeName].lastname,
  sbirthdate: state[storeName].birthdate,
});

const mapDispatchToProps = dispatch => ({
  userRegister: bindActionCreators(userRegisterAction, dispatch),
});

export const Register1 = connect(mapStateToProps, mapDispatchToProps)(Register1Container);
