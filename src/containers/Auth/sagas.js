import { takeLatest, put, call, select } from 'redux-saga/effects';
import { history } from '../../history';
import { USER_AUTHORIZE, USER_GET_AUTH_DATA, USER_LOGOUT, USER_REGISTER } from './constants';
import { authUserSuccessAction } from './actions';
import { getAuthKey, httpGet, httpPost } from '../../utils/http';
import { showSimpleError } from '../../utils/errors';
import { axios } from '../../utils/http';
import { storeName } from './reducer';

export const getUserData = state => state[storeName];

function* authUserSaga({ payload: { type, username, email, password } }) {
  try {
    if (type === 'login') {
      const user = yield httpPost('/auth/login', {
        username,
        password,
      });

      if (user.id) {
        yield put(authUserSuccessAction(user.id, user.first_name, user.last_name, user.city, user.birth_date, user.marital_status, user.political_status));

        // вот так вот
        localStorage.setItem('e-authkey', user.authKey);
        axios.defaults.headers.Authorization = getAuthKey();
        yield call(history.push, '/parties');
      } else {
        showSimpleError(user.errors);
      }
    }
  } catch (e) {
    console.error(e);
    yield call(history.push, '/login');
  }
}

function* userRegisterSaga({ payload }) {
  try {
    if (payload.isDone) {
      const userData = yield select(getUserData);

      const user = yield httpPost('/auth/sign-up', {
        first_name: userData.firstname,
        last_name: userData.lastname,
        city: userData.city,
        birth_date: userData.birthdate,
        marital_status: userData.marital_status,
        political_status: userData.political_status,

        email: userData.email,
        password: userData.password,
        code: userData.refcode,
      });

      if (user.id) {
        yield put(authUserSuccessAction(user.id, user.first_name, user.last_name, user.city, user.birth_date, user.marital_status, user.political_status));
        yield call(history.push, '/partycard');

        // вот так вот
        localStorage.setItem('e-authkey', user.authKey);
        axios.defaults.headers.Authorization = getAuthKey();
      } else {
        showSimpleError(user);
      }
    }
  } catch (e) {
    console.error(e);
  }
}

function* userGetAuthDataSaga() {
  try {
    const { id, first_name, last_name, city, birth_date, marital_status, political_status } = yield httpGet('/user/me');

    yield put(authUserSuccessAction(id, first_name, last_name, city, birth_date, marital_status, political_status));
  } catch (e) {
    console.error(e);
    // yield call(history.push, '/login');
  }
}

function* userLogoutSaga() {
  try {
    yield localStorage.removeItem('e-authkey');
    axios.defaults.headers.Authorization = '';
  } catch (e) {
    console.error(e);
  }
}

export function* authSagas() {
  yield takeLatest(USER_AUTHORIZE, authUserSaga);
  yield takeLatest(USER_REGISTER, userRegisterSaga);
  yield takeLatest(USER_GET_AUTH_DATA, userGetAuthDataSaga);
  yield takeLatest(USER_LOGOUT, userLogoutSaga);
}
