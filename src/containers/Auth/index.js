import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Container, Form, FormGroup, Input, Label } from 'reactstrap';
import { authUserAction } from './actions';
import { bindActionCreators } from 'redux';

export const AuthContainer = ({
  loginPage = false, registerPage = false, authUser,
}) => {
  const onSubmit = (e) => {
    e.preventDefault();
    const type = loginPage ? 'login' : 'register';
    authUser(type, username, email, password);
  };

  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  return (
    <Container>
      <h1>
        {loginPage && 'Вход'}
        {registerPage && 'Регистрация'}
      </h1>
      <Form onSubmit={onSubmit} autoComplete="off">
        <FormGroup>
          <Label for="exampleEmail">Логин</Label>
          <Input type="text" name="username"
                 placeholder="Введите логин"
                 value={username}
                 onChange={({ target }) => setUsername(target.value)}
          />
        </FormGroup>
        {
          registerPage && (
            <FormGroup>
              <Label for="exampleEmail">Email</Label>
              <Input type="email" name="email"
                     placeholder="Введите email"
                     value={email}
                     onChange={({ target }) => setEmail(target.value)}
              />
            </FormGroup>
          )
        }
        <FormGroup>
          <Label for="examplePassword">Пароль</Label>
          <Input type="password" name="password"
                 placeholder="Введите пароль"
                 value={password}
                 onChange={({ target }) => setPassword(target.value)}
          />
        </FormGroup>
        <Button type="submit" disabled={!(username.length && password.length)}>
          Войти
        </Button>
      </Form>
    </Container>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  authUser: bindActionCreators(authUserAction, dispatch),
});

export const Auth = connect(mapStateToProps, mapDispatchToProps)(AuthContainer);
