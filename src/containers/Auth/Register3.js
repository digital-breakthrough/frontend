import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Col, Container, Form, FormGroup, Input, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import { history } from '../../history';
import { userRegisterAction } from './actions';
import { bindActionCreators } from 'redux';
import { storeName } from './reducer';

export const Register3Container = ({ userRegister, scity, ssex, sworking, scompname, smarital_status }) => {
  const [city, setP1] = useState(scity);
  const [sex, setP2] = useState(ssex);
  const [working, setP3] = useState(sworking);
  const [compname, setP4] = useState(scompname);
  const [marital_status, setP5] = useState(smarital_status);

  return (
    <Container>
      <h1 className="h1">
        Регистрация на сайте: шаг 3
      </h1>
      <Row>
        <Col xs={5}>
          <Form autoComplete="off">
            <FormGroup>
              <Input type="text"
                     placeholder="Город проживания *"
                     value={city}
                     onChange={({ target }) => setP1(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="text"
                     placeholder="Семейное положение"
                     value={marital_status}
                     onChange={({ target }) => setP5(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="text"
                     placeholder="Пол"
                     value={sex}
                     onChange={({ target }) => setP2(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="text"
                     placeholder="Сфера деятельности"
                     value={working}
                     onChange={({ target }) => setP3(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="text"
                     placeholder="Название компании"
                     value={compname}
                     onChange={({ target }) => setP4(target.value)}
              />
            </FormGroup>
            <div className="mt-5 d-flex justify-content-between">
              <Button color="link" tag={Link} to="/register2">
                Вернуться
              </Button>
              <Button onClick={() => {
                        userRegister({
                          city, marital_status, sex, working, compname
                        });
                        history.push('/register4')
                      }}
                      color="primary"
                      type="submit" disabled={!(city.length)}
              >
                Далее
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

const mapStateToProps = state => ({
  scity: state[storeName].city,
  ssex: state[storeName].sex,
  sworking: state[storeName].working,
  scompname: state[storeName].compname,
  smarital_status: state[storeName].marital_status,
});

const mapDispatchToProps = dispatch => ({
  userRegister: bindActionCreators(userRegisterAction, dispatch),
});

export const Register3 = connect(mapStateToProps, mapDispatchToProps)(Register3Container);
