import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Col, Container, Form, FormGroup, Input, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import { history } from '../../history';
import { userRegisterAction } from './actions';
import { bindActionCreators } from 'redux';
import { storeName } from './reducer';

export const Register4Container = ({ userRegister, sspass, snpass, sdatepass, scodepass, swhompass }) => {
  const [spass, setP0] = useState(sspass);
  const [npass, setP1] = useState(snpass);
  const [datepass, setP2] = useState(sdatepass);
  const [codepass, setP3] = useState(scodepass);
  const [whompass, setP4] = useState(swhompass);

  return (
    <Container>
      <h1 className="h1">
        Регистрация на сайте: шаг 4
      </h1>
      <Row>
        <Col xs={5}>
          <Form autoComplete="off">
            <Row>
              <Col xs={5}>
                <FormGroup>
                  <Input type="text"
                         placeholder="Серия паспорта"
                         value={spass}
                         onChange={({ target }) => setP0(target.value)}
                  />
                </FormGroup>
              </Col>
              <Col xs={7}>
                <FormGroup>
                  <Input type="text"
                         placeholder="Номер паспорта"
                         value={npass}
                         onChange={({ target }) => setP1(target.value)}
                  />
                </FormGroup>
              </Col>
            </Row>
            <FormGroup>
              <Input type="text"
                     placeholder="Дата выдачи"
                     value={datepass}
                     onChange={({ target }) => setP2(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="text"
                     placeholder="Код подразделения"
                     value={codepass}
                     onChange={({ target }) => setP3(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="text"
                     placeholder="Кем выдан"
                     value={whompass}
                     onChange={({ target }) => setP4(target.value)}
              />
            </FormGroup>
            <div className="mt-5 d-flex justify-content-between">
              <Button color="link" tag={Link} to="/register3">
                Вернуться
              </Button>
              <Button onClick={() => {
                        userRegister({
                          spass, npass, datepass, codepass, whompass
                        });
                        history.push('/register5')
                      }}
                      color="primary"
                      type="submit"
              >
                Далее
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

const mapStateToProps = state => ({
  sspass: state[storeName].spass,
  snpass: state[storeName].npass,
  sdatepass: state[storeName].datepass,
  scodepass: state[storeName].codepass,
  swhompass: state[storeName].whompass,
});

const mapDispatchToProps = dispatch => ({
  userRegister: bindActionCreators(userRegisterAction, dispatch),
});

export const Register4 = connect(mapStateToProps, mapDispatchToProps)(Register4Container);
