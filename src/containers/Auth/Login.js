import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Col, Container, Form, FormGroup, Input, Row } from 'reactstrap';
import { authUserAction } from './actions';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';

export const LoginContainer = ({
  authUser,
}) => {
  const onSubmit = (e) => {
    e.preventDefault();
    authUser('login', username, '', password);
  };

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  return (
    <Container>
      <h1 className="h1">
        Авторизация на сайте
      </h1>
      <Row>
        <Col xs={5}>
          <Form onSubmit={onSubmit} autoComplete="off">
            <FormGroup>
              <Input type="text" name="username"
                     placeholder="Логин (email или телефон)"
                     value={username}
                     onChange={({ target }) => setUsername(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="password" name="password"
                     placeholder="Введите пароль"
                     value={password}
                     onChange={({ target }) => setPassword(target.value)}
              />
            </FormGroup>
            <div className="mt-5 d-flex justify-content-between">
              <Button color="primary" type="submit" disabled={!(username.length && password.length)}>
                Войти
              </Button>
              <Button color="link" tag={Link} to="/register1">
                Зарегистрироваться
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  authUser: bindActionCreators(authUserAction, dispatch),
});

export const Login = connect(mapStateToProps, mapDispatchToProps)(LoginContainer);
