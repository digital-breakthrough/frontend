import {
  BECOME_CITIZEN,
  USER_AUTHORIZE,
  USER_AUTHORIZE_ERROR,
  USER_AUTHORIZE_SUCCESS,
  USER_GET_AUTH_DATA, USER_LOGOUT, USER_REGISTER
} from './constants';

export const authUserAction = (type, username, email, password) => ({
  type: USER_AUTHORIZE,
  payload: {
    type,
    username,
    email,
    password,
  },
});

export const authUserSuccessAction = (id, firstname, lastname, city, birth_date, marital_status, political_status) => ({
  type: USER_AUTHORIZE_SUCCESS,
  payload: {
    id,
    firstname,
    lastname,
    city,
    birth_date,
    marital_status,
    political_status,
  },
});

export const authUserErrorAction = () => ({
  type: USER_AUTHORIZE_ERROR,
  payload: {},
});

export const getUserDataAction = () => ({
  type: USER_GET_AUTH_DATA,
  payload: {},
});

export const becomeCitizenAction = () => ({
  type: BECOME_CITIZEN,
  payload: {},
});

export const logoutAction = () => ({
  type: USER_LOGOUT,
  payload: {},
});

export const userRegisterAction = (data, isDone = false) => ({
  type: USER_REGISTER,
  payload: {
    ...data,
    isDone,
  },
});
