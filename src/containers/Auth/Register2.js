import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Col, Container, Form, FormGroup, Input, Row } from 'reactstrap';
import { Link } from 'react-router-dom';
import { history } from '../../history';
import { userRegisterAction } from './actions';
import { bindActionCreators } from 'redux';
import { storeName } from './reducer';

export const Register2Container = ({ userRegister, semail, sphone, spassword }) => {
  const [email, setP1] = useState(semail);
  const [phone, setP2] = useState(sphone);
  const [password, setP3] = useState(spassword);
  const [p4, setP4] = useState('');

  return (
    <Container>
      <h1 className="h1">
        Регистрация на сайте: шаг 2
      </h1>
      <Row>
        <Col xs={5}>
          <Form autoComplete="off">
            <FormGroup>
              <Input type="text"
                     placeholder="Email *"
                     value={email}
                     onChange={({ target }) => setP1(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="text"
                     placeholder="Телефон"
                     value={phone}
                     onChange={({ target }) => setP2(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="password"
                     placeholder="Пароль *"
                     value={password}
                     onChange={({ target }) => setP3(target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Input type="password"
                     placeholder="Повторите пароль *"
                     value={p4}
                     onChange={({ target }) => setP4(target.value)}
              />
            </FormGroup>
            <div className="mt-5 d-flex justify-content-between">
              <Button color="link" tag={Link} to="/register1">
                Вернуться
              </Button>
              <Button onClick={() => {
                        userRegister({
                          email, phone, password
                        });
                        history.push('/register3')
                      }}
                      color="primary"
                      type="submit" disabled={!(email.length && password.length && p4.length)}
              >
                Далее
              </Button>
            </div>
          </Form>
        </Col>
      </Row>
    </Container>
  );
};

const mapStateToProps = state => ({
  semail: state[storeName].email,
  sphone: state[storeName].phone,
  spassword: state[storeName].password,
});

const mapDispatchToProps = dispatch => ({
  userRegister: bindActionCreators(userRegisterAction, dispatch),
});

export const Register2 = connect(mapStateToProps, mapDispatchToProps)(Register2Container);
