import { CREATE_PARTY, GET_PARTIY_CARD, GET_PARTIY_CARD_SUCCESS } from './constants';

export const createPartyAction = (name) => ({
  type: CREATE_PARTY,
  payload: {
    name,
  },
});

export const getPartyCardAction = (id) => ({
  type: GET_PARTIY_CARD,
  payload: {
    id,
  },
});

export const getPartyCardSuccessAction = (data) => ({
  type: GET_PARTIY_CARD_SUCCESS,
  payload: {
    data,
  },
});
