
import {
  GET_PARTIY_CARD_SUCCESS
} from './constants';

const initialState = {
  isLoading: true,
  data: {},
};

export const storeName = 'party';

export const party = {
  [storeName]: (state = initialState, { type, payload }) => {
    switch (type) {
      case GET_PARTIY_CARD_SUCCESS:
        return {
          ...state,
          isLoading: false,
          data: payload.data,
        };
      default:
        return state;
    }
  },
};
