import { takeLatest, call, put } from 'redux-saga/effects';
import { CREATE_PARTY, GET_PARTIY_CARD } from './constants';
import { httpGet, httpPost } from '../../utils/http';
import { history } from '../../history';
import { showSimpleError } from '../../utils/errors';
import { getPartyCardSuccessAction } from './actions';

function* createPartySaga({ payload: { name } }) {
  try {
    const response = yield httpPost('/consignment', {
      name,
    });

    if (response) {
      yield call(history.push, `/party/${response.id}`);
    } else {
      showSimpleError(response);
    }
  } catch (e) {
    console.error(e);
  }
}

function* getPartySaga({ payload: { id } }) {
  try {
    const response = yield httpGet(`/consignment/${id}`, {
      expand: 'members'
    });

    yield put(getPartyCardSuccessAction(response));
  } catch (e) {
    console.error(e);
  }
}

export function* partySagas() {
  yield takeLatest(CREATE_PARTY, createPartySaga);
  yield takeLatest(GET_PARTIY_CARD, getPartySaga);
}
