import React from 'react';
import { connect } from 'react-redux';

export const PartyCardContainer = () => {
  return (
    <div />
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

export const PartyCard = connect(mapStateToProps, mapDispatchToProps)(PartyCardContainer);
