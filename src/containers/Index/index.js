import React from 'react';
import { connect } from 'react-redux';

export const IndexContainer = () => {
  return (
    <div />
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

export const Index = connect(mapStateToProps, mapDispatchToProps)(IndexContainer);
