import React from 'react';
import { connect } from 'react-redux';
import { Button, Col, Row } from 'reactstrap';
import { storeName as authStoreName } from '../Auth/reducer';
import { becomeCitizenAction } from '../Auth/actions';
import { bindActionCreators } from 'redux';

export const ProfileContainer = ({
 isCitizen, firstname, lastname, city, birth_date, marital_status, political_status, becomeCitizen,
}) => {
  return (
    <Row className="mt-3 profile">
      <Col xs={5}>
        <div className="photo" />
        <div className="friends mt-3">
          <p className="text-center mb-3"><strong>ДРУЗЬЯ 254</strong></p>
          <div className="d-flex justify-content-between">
            <div>
              <div>
                <div className="img" />
                <p>Леонид Рязанов</p>
              </div>
              <div className="mt-2">
                <div className="img" />
                <p>Леонид Рязанов</p>
              </div>
            </div>
            <div>
              <div>
                <div className="img" />
                <p>Леонид Рязанов</p>
              </div>
              <div className="mt-2">
                <div className="img" />
                <p>Леонид Рязанов</p>
              </div>
            </div>
          </div>
        </div>
      </Col>
      <Col xs={7} className="p-0">
        <div>
          <strong>{isCitizen ? 'гражданин' : 'турист'}</strong>
          {!isCitizen && <Button color="link" onClick={becomeCitizen}>Попробовать (стать гражданином)</Button>}
        </div>
        <h1><strong>{firstname} {lastname}</strong></h1>
        <p>Город: {city}</p>
        <p>Дата рождения: {birth_date}</p>
        <p>Семейное положение: {marital_status}</p>
        <p>Политические убеждения: {political_status}</p>
        <p className="text-underline showmore">Показать подробную информацию</p>
        <div className="d-flex justify-content-between text-center mini-stat">
          <div><strong>212</strong><span>друзей</span></div>
          <div><strong>12</strong><span>инициатив</span></div>
          <div><strong>21</strong><span>раз голосовал</span></div>
          <div><strong>2</strong><span>член партии</span></div>
        </div>
        <p className="posts mt-3 font-weight-bold">Все записи</p>
      </Col>
    </Row>
  );
};

const mapStateToProps = state => ({
  isCitizen: state[authStoreName].isCitizen,
  firstname: state[authStoreName].firstname,
  lastname: state[authStoreName].lastname,
  city: state[authStoreName].city,
  birth_date: state[authStoreName].birthdate,
  marital_status: state[authStoreName].marital_status,
  political_status: state[authStoreName].political_status,
});

const mapDispatchToProps = dispatch => ({
  becomeCitizen: bindActionCreators(becomeCitizenAction, dispatch),
});

export const Profile = connect(mapStateToProps, mapDispatchToProps)(ProfileContainer);
