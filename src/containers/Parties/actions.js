import { GET_PARTIES, GET_PARTIES_SUCCESS } from './constants';

export const getPartiesAction = () => ({
  type: GET_PARTIES,
  payload: {},
});

export const getPartiesSuccessAction = (arr) => ({
  type: GET_PARTIES_SUCCESS,
  payload: {
    arr
  },
});
