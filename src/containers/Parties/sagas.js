import { takeLatest, put } from 'redux-saga/effects';
import { GET_PARTIES } from './constants';
import { httpGet } from '../../utils/http';
import { getPartiesSuccessAction } from './actions';

function* getPartiesSaga() {
  try {
    const response = yield httpGet('/consignment');

    yield put(getPartiesSuccessAction(response));
  } catch (e) {
    console.error(e);
  }
}

export function* partiesSagas() {
  yield takeLatest(GET_PARTIES, getPartiesSaga);
}
