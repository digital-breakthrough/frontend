import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { PartiesComponent } from '../../components/Parties';
import { bindActionCreators } from 'redux';
import { getPartiesAction } from './actions';
import { storeName } from './reducer';
import { storeName as authStoreName } from '../Auth/reducer';

export const PartiesContainer = ({
 getParties, parties, isCitizen,
}) => {
  useEffect(() => {
    getParties()
  }, [getParties]);

  return (
    <PartiesComponent parties={parties} isCitizen={isCitizen} />
  );
};

const mapStateToProps = state => ({
  parties: state[storeName].data,
  isCitizen: state[authStoreName].isCitizen,
});

const mapDispatchToProps = dispatch => ({
  getParties: bindActionCreators(getPartiesAction, dispatch),
});

export const Parties = connect(mapStateToProps, mapDispatchToProps)(PartiesContainer);
