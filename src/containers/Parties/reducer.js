
import {
  GET_PARTIES_SUCCESS
} from './constants';

const initialState = {
  isLoading: true,
  data: [],
};

export const storeName = 'parties';

export const parties = {
  [storeName]: (state = initialState, { type, payload }) => {
    switch (type) {
      case GET_PARTIES_SUCCESS:
        return {
          ...state,
          isLoading: false,
          data: payload.arr,
        };
      default:
        return state;
    }
  },
};
