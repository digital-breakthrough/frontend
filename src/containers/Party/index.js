import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getPartyCardAction } from '../CreateParty/actions';
import { storeName } from '../CreateParty/reducer';
import { storeName as authStoreName } from '../Auth/reducer';
import { PartyComponent } from '../../components/Party';
import { becomeCitizenAction } from '../Auth/actions';

export const PartyContainer = ({ match, getPartyCard, party, becomeCitizen, isCitizen }) => {
  useEffect(() => {
    getPartyCard(match.params.id)
  }, [getPartyCard, match.params.id]);

  return (
    <PartyComponent party={party} becomeCitizen={becomeCitizen} isCitizen={isCitizen} />
  );
};

const mapStateToProps = state => ({
  party: state[storeName].data,
  isCitizen: state[authStoreName].isCitizen,
});

const mapDispatchToProps = dispatch => ({
  getPartyCard: bindActionCreators(getPartyCardAction, dispatch),
  becomeCitizen: bindActionCreators(becomeCitizenAction, dispatch),
});

export const Party = connect(mapStateToProps, mapDispatchToProps)(PartyContainer);
