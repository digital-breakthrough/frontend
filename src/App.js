import React, { useEffect } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Login } from './containers/Auth/Login';
import { Register1 } from './containers/Auth/Register1';
import { PrivateRoute } from './utils/PrivateRoute';
import { NavMenu } from './components/Nav';
import { Index } from './containers/Index';
import { Page404 } from './components/404';
import { PartyCard } from './containers/PartyCard';
import { getUserDataAction, logoutAction } from './containers/Auth/actions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { storeName } from './containers/Auth/reducer';
import { Sidebar } from './components/Nav/Sidebar';
import { CreateParty } from './components/CreateParty';
import { Party } from './containers/Party';
import { Parties } from './containers/Parties';
import { Register2 } from './containers/Auth/Register2';
import { Register3 } from './containers/Auth/Register3';
import { Register4 } from './containers/Auth/Register4';
import { Register5 } from './containers/Auth/Register5';
import { Profile } from './containers/Profile';
import { Col, Row } from 'reactstrap';
import Container from 'reactstrap/es/Container';
import { Votes } from './components/Votes';

export const AppContainer = ({
 id, getUserData, logout,
}) => {
  useEffect(() => {
    if (id === 0) {
      getUserData()
    }
  }, [id, getUserData]);

  return (
    <>
      {
        (id > 0) ? (
          <>
            <NavMenu logout={logout} />
            <Container>
              <Row>
                <Col xs={3}>
                  <Sidebar />
                </Col>
                <Col xs={7}>
                  <Switch>
                    <Route exact path="/" component={Profile} />
                    <Route path="/partycard" component={PartyCard} />
                    <Route path="/votes" component={Votes} />
                    <Route path="/createparty" component={CreateParty} />
                    <Route path="/parties" component={Parties} />
                    <Route path="/party/:id" component={Party} />
                    <PrivateRoute exact path="/private" component={Login} />
                    <Route path="*" component={Page404} />
                  </Switch>
                </Col>
                <Col xs={2}>
                </Col>
              </Row>
            </Container>
          </>
        ) : (
          <>
            <NavMenu />
            <Switch>
              <Route exact path="/" component={Index} />
              <Route path="/login" component={Login} />
              <Route path="/register1" component={Register1} />
              <Route path="/register2" component={Register2} />
              <Route path="/register3" component={Register3} />
              <Route path="/register4" component={Register4} />
              <Route path="/register5" component={Register5} />
            </Switch>
          </>
        )
      }
    </>
  );
};

const mapStateToProps = state => ({
  id: state[storeName].id,
});

const mapDispatchToProps = dispatch => ({
  getUserData: bindActionCreators(getUserDataAction, dispatch),
  logout: bindActionCreators(logoutAction, dispatch),
});

export const App = connect(mapStateToProps, mapDispatchToProps)(AppContainer);
