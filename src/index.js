import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.css';
import './index.scss';

import configureStore from './utils/configureStore';
import { history } from './history';
import { App } from './App';

export const store = configureStore(history);

// это только прототип.
// ни одна из используемых идей не является истиной и в настоящем проекте возможны значительные изменения

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <App />
    </Router>
  </Provider>
, document.getElementById('root'));
