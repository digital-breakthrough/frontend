export const showSimpleError = (errors) => {
  alert(Object.keys(errors).map(e => errors[e]).join('\n'));
};
