import ajax from 'axios';

export const getAuthKey = () => {
  const isKey = localStorage.getItem('e-authkey');
  return `Bearer ${isKey}`;
};

export const axios = ajax.create({
  baseURL: 'http://api.е-страна.com',
  headers: { Authorization: getAuthKey() },
});

axios.interceptors.response.use(data => data.data,
  (error) => {
    return Promise.reject(error);
  });

export const httpGet = (path, params = {}) => axios.get(
  path,
  {
    withCredentials: true,
    params,
  },
);

export const httpPost = (path, data = {}) => axios.post(
  path,
  data,
);

export const httpDelete = (path) => axios.delete(
  path,
);
