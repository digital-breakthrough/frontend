import React from 'react';
import { connect } from 'react-redux';
import { storeName } from '../containers/Auth/reducer';
import { Redirect, Route } from 'react-router-dom';

export const PrivateRouteContainer = ({
  isAuth, ...rest
}) => {
  return (
    <>
      {!isAuth && <Redirect to='/login' />}
      <Route {...rest} />
    </>
  );
};

const mapStateToProps = state => ({
  isAuth: state[storeName].isAuth,
});

export const PrivateRoute = connect(mapStateToProps)(PrivateRouteContainer);
