import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';

import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

import { reducers } from '../store/reducers';
import { rootSagas } from '../store/sagas';

export default function storeConfigure(history) {
  const sagaMiddleware = createSagaMiddleware({
    context: {
      history,
    },
    onError(err) {
      console.error(err);
    },
  });

  const composeEnhancers = (...args)  =>{
    const compose = composeWithDevTools({
      name: 'e-strana-devtools',
      trace: true,
    });

    return compose(...args);
  };

  const enhancer = composeEnhancers(
    applyMiddleware(
      sagaMiddleware,
    )
  );

  const store = createStore(reducers, enhancer);

  sagaMiddleware.run(rootSagas);

  return store;
}
