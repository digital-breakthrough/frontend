import React from 'react';
import { Button, Col, Row } from 'reactstrap';
import logop from '../../assets/logop.jpg';

export const PartyComponent = ({
 party, isCitizen,
}) => {
  return (
    <div className="p-card">
      <h1 className="inner-header color-main">{party.name}</h1>
      <div>
        <div className="tag">
          Парламентская
        </div>
        <img src={logop} alt="Лого партии" width="100%" className="mb-3" />
      </div>
      <div className="float-right">
        <Button color="link">Поддержать</Button>
        <Button color="link">Написать</Button>
        <Button color="primary">Выйти из партии</Button>
        <Button color="link">Еще</Button>
      </div>
      <div className="clearfix" />
      <Row>
        <Col xs={7}>
          <p className="info color-main">
            Информация о партии
          </p>
          <div>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam animi architecto culpa debitis, doloremque
            esse in inventore laudantium odio odit porro quaerat quis rem repudiandae sed vel voluptate. Doloremque eaque
            eveniet excepturi in nisi placeat quam ullam. A, dicta dolorem est exercitationem fugiat, neque nihil nisi
            provident temporibus ut velit.
          </div>
          <p className="text-underline showmore mt-3">СКРЫТЬ ПОДРОБНУЮ ИНФОРМАЦИЮ</p>
          <div className="friends big mt-3">
            <p className="text-center head">
              <strong>УПРАВЛЕНИЕ ПАРТИИ</strong>
            </p>
            <Row className="d-flex justify-content-between">
              <Col>
                <div>
                  <div className="img" />
                  <p><strong>ЗЮГАНОВ Г.А.</strong></p>
                  <p className="card-subtitle">председатель</p>
                </div>
              </Col>
              <Col>
                <div>
                  <div className="img" />
                  <p><strong>КАШИН В.И.</strong></p>
                  <p className="card-subtitle">первый зам</p>
                </div>
              </Col>
            </Row>
          </div>
          {/*{*/}
          {/*  party.members && (*/}
          {/*    <Card body style={{ marginTop: 30 }}>*/}
          {/*      <CardTitle>*/}
          {/*        Управление партии*/}
          {/*      </CardTitle>*/}
          {/*      <CardText tag="div">*/}
          {/*        <Row>*/}
          {/*          {*/}
          {/*            party.members.map(m => (*/}
          {/*              <Col xs={2} className="text-center" key={m.user.id}>*/}
          {/*                <img src={m.user.avatar} alt="" style={{ width: '100%' }} />*/}
          {/*                <h6 className="card-title">{m.user.userName}</h6>*/}
          {/*                <h6 className="card-subtitle mb-2 text-muted">Должность</h6>*/}
          {/*              </Col>*/}
          {/*            ))*/}
          {/*          }*/}
          {/*        </Row>*/}
          {/*      </CardText>*/}
          {/*    </Card>*/}
          {/*  )*/}
          {/*}*/}
        </Col>
        <Col xs={5}>
          <div className="friends mt-3">
            <p className="text-center mb-3">
              <strong>СТОРОННИКИ ПАРТИИ</strong>
            </p>
            <p className="text-center mb-3">
              <strong>254 052 123</strong> (4 друзей)
            </p>
            <div className="d-flex justify-content-between">
              <div>
                <div>
                  <div className="img" />
                  <p>Леонид Рязанов</p>
                </div>
                <div className="mt-2">
                  <div className="img" />
                  <p>Леонид Рязанов</p>
                </div>
                <div className="mt-2">
                  <div className="img" />
                  <p>Леонид Рязанов</p>
                </div>
              </div>
              <div>
                <div>
                  <div className="img" />
                  <p>Леонид Рязанов</p>
                </div>
                <div className="mt-2">
                  <div className="img" />
                  <p>Леонид Рязанов</p>
                </div>
                <div className="mt-2">
                  <div className="img" />
                  <p>Леонид Рязанов</p>
                </div>
              </div>
            </div>
          </div>
          <div className="friends mt-3">
            <p className="text-center mb-3">
              <strong>ЧЛЕНЫ ПАРТИИ</strong>
            </p>
            <p className="text-center mb-3">
              <strong>4 052 123</strong> (12 друзей)
            </p>
            <div className="d-flex justify-content-between">
              <div>
                <div>
                  <div className="img" />
                  <p>Леонид Рязанов</p>
                </div>
                <div className="mt-2">
                  <div className="img" />
                  <p>Леонид Рязанов</p>
                </div>
                <div className="mt-2">
                  <div className="img" />
                  <p>Леонид Рязанов</p>
                </div>
              </div>
              <div>
                <div>
                  <div className="img" />
                  <p>Леонид Рязанов</p>
                </div>
                <div className="mt-2">
                  <div className="img" />
                  <p>Леонид Рязанов</p>
                </div>
                <div className="mt-2">
                  <div className="img" />
                  <p>Леонид Рязанов</p>
                </div>
              </div>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};
