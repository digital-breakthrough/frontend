import React, { useState } from 'react';
import { Button, Col, Form, FormGroup, Input, Label } from 'reactstrap';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { createPartyAction } from '../../containers/CreateParty/actions';

export const CreatePartyContainer = ({
 createParty
}) => {
  const onSubmit = (e) => {
    e.preventDefault();

    createParty(partyName);
  };

  const [partyName, setPartyName] = useState('');
  const [partyDescr, setPartyDescr] = useState('');
  const [l1, setL1] = useState('');
  const [l2, setL2] = useState('');
  const [l3, setL3] = useState('');
  const [l4, setL4] = useState('');
  const [l5, setL5] = useState('');

  return (
    <div>
      <h1 className="inner-header">Создание партии</h1>
      <Form onSubmit={onSubmit} autoComplete="off">
        <FormGroup row>
          <Label xs={5}>Введите название партии</Label>
          <Col xs={7}>
            <Input type="text" name="party_name" value={partyName}
                   onChange={({ target }) => setPartyName(target.value)}
            />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label xs={5}>Введите описание партии</Label>
          <Col xs={7}>
            <Input type="textarea" name="party_descr" value={partyDescr}
                   onChange={({ target }) => setPartyDescr(target.value)}
            />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label xs={5}>Укажите ссылки на управляющие партии</Label>
          <Col xs={7}>
            <Input type="text" value={l1} onChange={({ target }) => setL1(target.value)} />
            <Input type="text" value={l2} onChange={({ target }) => setL2(target.value)} className="mt-3" />
            <Input type="text" value={l3} onChange={({ target }) => setL3(target.value)} className="mt-3" />
          </Col>
        </FormGroup>
        <FormGroup tag="fieldset" row>
          <h5>Вступление в партию</h5>
          <Col sm={10}>
            <FormGroup check>
              <Label check>
                <Input type="radio" name="radio2" checked />
                Автоматическое без испытательного срока
              </Label>
            </FormGroup>
            <FormGroup check>
              <Label check>
                <Input type="radio" name="radio2" />
                Автоматическое c испытательнмы сроком
              </Label>
            </FormGroup>
            <FormGroup check disabled>
              <Label check>
                <Input type="radio" name="radio2" disabled />
                Через заявку с испытательным сроком
              </Label>
            </FormGroup>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label xs={12}>Продолжительность испытательного срока</Label>
          <Col xs={7}>
            <Input type="text" value={l4} onChange={({ target }) => setL4(target.value)}
                   placeholder="Укажите продолжительность испытательного срока"
                   className="mini-placeholder"
            />
          </Col>
          <Col xs={3}>
            <Input type="select" value={l5} onChange={({ target }) => setL5(target.value)}>
              <option>Месяцев</option>
              <option>Дней</option>
            </Input>
          </Col>
        </FormGroup>
        <div className="col-6 d-flex justify-content-between" style={{ float: 'right' }}>
          <Button color="link">
            Отмена
          </Button>
          <Button type="submit" disabled={!(partyName.length && partyDescr.length)}>
            Сохранить
          </Button>
        </div>
      </Form>
    </div>
  );
};

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
  createParty: bindActionCreators(createPartyAction, dispatch),
});

export const CreateParty = connect(mapStateToProps, mapDispatchToProps)(CreatePartyContainer);
