import React from 'react';
import { Button, ButtonGroup, Card, CardText, CardTitle, Col, Row } from 'reactstrap';
import CardLink from 'reactstrap/es/CardLink';
import { NavLink } from 'react-router-dom';

export const PartiesComponent = ({
 parties, isCitizen,
}) => {
  return (
    <Row>
      {
        parties.map(p => (
          <Col xs={6} key={p.id} style={{ marginBottom: 30 }}>
            <Card body>
              <CardTitle className="text-center">
                <div style={{ width: '100%', height: 120, background: 'aliceblue' }} />
                <CardLink tag={NavLink} to={`/party/${p.id}`} className="d-block" style={{ margin: '10px 0px -10px 0' }}>
                  {p.name}
                </CardLink>
              </CardTitle>
              <CardText tag="div">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab alias aliquid amet aperiam asperiores autem
                  consequuntur distinctio dolorum eaque enim error excepturi in minima molestias nam, nemo nisi nobis obcaecati
                </p>
                <div className="d-flex justify-content-between">
                  <div>
                    32 места в госдуме
                  </div>
                  <div>
                    9 353 000
                  </div>
                </div>
                <div className="d-flex justify-content-end">
                  4 друзей
                </div>
              </CardText>
              {
                isCitizen && (
                  <ButtonGroup>
                    <Button color="link">Написать</Button>
                    <Button color="primary">Выйти из партии</Button>
                  </ButtonGroup>
                )
              }
            </Card>
          </Col>
        ))
      }
    </Row>
  );
};
