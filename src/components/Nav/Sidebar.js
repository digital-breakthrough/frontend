import React from 'react';
import {  NavLink } from 'react-router-dom';

export const Sidebar = () => (
  <nav id="sidebar">
    <ul className="list-unstyled components">
      <li>
        <NavLink to="/" exact>Моя страница</NavLink>
        <NavLink to="/votes">Мои голоса</NavLink>
        <NavLink to="/friends">Мои друзья</NavLink>
        <NavLink to="/bookmarks">Закладки</NavLink>
        <NavLink to="/feed">Новости друзей</NavLink>
        <NavLink to="/refprogramm">Реферальная программа</NavLink>
        <NavLink to="/appeals">Мои обращения</NavLink>
        <NavLink to="/createparty">Создать партию</NavLink>
      </li>
    </ul>
  </nav>
);
