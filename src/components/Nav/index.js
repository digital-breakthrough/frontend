import React, { useState } from 'react';
import { Nav, NavItem, NavLink as RNavLink } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import Container from 'reactstrap/es/Container';
import Input from 'reactstrap/es/Input';
import logo from '../../assets/logo.png';
import ReactFlagsSelect from 'react-flags-select';

export const NavMenu = ({ logout }) => {
  const [country, setCountry] = useState('RU');

  return (
    <div className="menu-nav">
      <Container className="d-flex">
        <NavLink to="/" className="navbar-brand">
          <img src={logo} alt="лого" width={120} />
        </NavLink>
        <ReactFlagsSelect
          countries={["RU", "UA", "KZ", "US"]}
          placeholder="Select Language"
          showSelectedLabel={false}
          showOptionLabel={false}
          defaultCountry={country}
          onSelect={setCountry}
        />
        <Nav pills>
          <NavItem>
            <RNavLink tag={NavLink} to="/votes">Выборы</RNavLink>
          </NavItem>
          <NavItem>
            <RNavLink tag={NavLink} to="/parties">Партии</RNavLink>
          </NavItem>
          <NavItem>
            <RNavLink tag={NavLink} to="/2">Инициативы</RNavLink>
          </NavItem>
          <NavItem>
            <RNavLink tag={NavLink} to="/3">Биржа</RNavLink>
          </NavItem>
          <NavItem>
            <RNavLink tag={NavLink} to="/4">6 коин</RNavLink>
          </NavItem>
          {
            logout ? (
              <NavItem>
                <RNavLink tag={NavLink} to="/logout" onClick={(e) => {
                  e.preventDefault();
                  logout();
                }}>Выход</RNavLink>
              </NavItem>
            ) : (
              <>
                <NavItem>
                  <RNavLink tag={NavLink} to="/register1">Регистрация</RNavLink>
                </NavItem>
                <NavItem>
                  <RNavLink tag={NavLink} to="/login">Вход</RNavLink>
                </NavItem>
              </>
            )
          }
          <NavItem>
            <Input type="text" placeholder="Поиск" className="search" />
          </NavItem>
        </Nav>
      </Container>
    </div>
  );
};
