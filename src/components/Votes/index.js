import React from 'react';
import { Button, Col, Row } from 'reactstrap';

export const Votes = () => (
  <div className="votes">
    <h1 className="inner-header">Выборы</h1>
    <div className="text-center">
      <Button color="primary">
        Выдвинуть свою кандидатуру
      </Button>
    </div>
    <div className="vote-block">
      <div className="d-flex justify-content-between">
        <div><strong>Выборы в государственную думу РФ</strong></div>
        <div>
          Начнутся 19 августа 2019
        </div>
      </div>
      <Row className=" mt-3">
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
        </Col>
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
        </Col>
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
        </Col>
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
        </Col>
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
        </Col>
      </Row>
      <div className="text-center show-all text-muted">Посмотреть всех кандидатов</div>
    </div>
    <div className="vote-block">
      <div className="d-flex justify-content-between">
        <div><strong>Выборы президента Российской Федерации</strong></div>
        <div>
          Идут
        </div>
      </div>
      <Row className=" mt-3">
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
          <p>5 голосов</p>
        </Col>
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
          <p>5 голосов</p>
        </Col>
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
          <p>5 голосов</p>
        </Col>
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
          <p>5 голосов</p>
        </Col>
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
          <p>5 голосов</p>
        </Col>
      </Row>
      <div className="text-center show-all text-muted">Посмотреть всех и переголосовать</div>
    </div>
    <div className="vote-block">
      <div className="d-flex justify-content-between">
        <div><strong>Выборы в краевое законодательное собрание</strong></div>
        <div>
          Завершены
        </div>
      </div>
      <Row className=" mt-3">
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
          <p>20% голосов</p>
        </Col>
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
          <p>20% голосов</p>
        </Col>
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
          <p>20% голосов</p>
        </Col>
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
          <p>20% голосов</p>
        </Col>
        <Col>
          <div className="img" />
          <p>Кандидат</p>
          <p className="sub">Партия</p>
          <p>20% голосов</p>
        </Col>
      </Row>
      <div className="text-center show-all text-muted">Посмотреть результаты</div>
    </div>
  </div>
);
