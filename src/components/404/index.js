import React from 'react';

export const Page404 = () => {
  return (
    <>
      <h1>
        404
      </h1>
      <h3>Кажется, это не получилось сделать...</h3>
    </>
  );
};
